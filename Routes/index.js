const Router = require('koa-router')
const combineRouters = require('koa-combine-routers')
const getMovieByName = require('./services/getMovieByName')
const getAllDbMovies = require('./services/getAllDbMovies')
const postAndReplace = require('./services/postAndReplace')
const movierouter = new Router()

movierouter.get('/movie', getMovieByName);

movierouter.get('/allmovies', getAllDbMovies);

movierouter.post('/replace', postAndReplace)


const router = combineRouters(
    movierouter,
  )

module.exports = router;