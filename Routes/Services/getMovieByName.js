const Movie = require("../../models/Movie");
const axios = require("axios");

const getMovieByName = async (ctx) => {
  try {
    let arr = [];
    const { movieName } = ctx.request.query;
    const movieYear = ctx.request.headers.year;
    console.log;
    if (!movieName) {
      ctx.status = 409;
      ctx.body = {
        message: "Movie name is required",
      };
    }
    await axios.get(`https://www.omdbapi.com/?apikey=204bb3a1&t=${movieName}&y=${movieYear}`).then((res) => {
      arr.push(res.data);
    });
    const movie = await Movie.findOne({ title: arr[0].Title });
    if (!movie) {
      const newMovie = new Movie({
        title: arr[0].Title,
        year: arr[0].Year,
        released: arr[0].Released,
        plot: arr[0].Plot,
        actors: arr[0].Actors,
        genre: arr[0].Genre,
        director: arr[0].Director,
        ratings: arr[0].Ratings.concat.apply(arr[0].Ratings.map((el) => el.Value + " , " + el.Source)),
      });
      newMovie.save();
      ctx.body = newMovie;
    } else {
      ctx.body = movie;
    }
  } catch (error) {
    ctx.status = 409;
    ctx.body = error;
  }
};

module.exports = getMovieByName;
