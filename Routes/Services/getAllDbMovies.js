const Movie = require("../../models/Movie");

const getAllDbMovies = async (ctx) => {
  const pageNumber = ctx.request.headers.page;
  const limit = 5;
  try {
    const movies = await Movie.find()
      .limit(limit)
      .skip(limit * (pageNumber - 1));
    const count = await Movie.countDocuments();
    ctx.body = { movies, totalPages: Math.ceil(count / limit), currentPage: pageNumber };
  } catch (err) {
    ctx.body = { message: err };
  }
};

module.exports = getAllDbMovies;
