const Movie = require("../../models/Movie");

const postAndReplace = async (ctx) => {
    let newPlot = ''
  const obj = ctx.request.body;
  try {
      await Movie.findOne({ title: obj.movie}).then((movie) => {
          newPlot = movie.plot.replaceAll(obj.find, obj.replace);
      })
    await Movie.findOneAndUpdate({ title: obj.movie }, {plot: newPlot}).then((movie) => {
      ctx.body = movie.plot.replaceAll(obj.find, obj.replace);
      
    });
  } catch (err) {
    ctx.status = 409;
    ctx.body = err;
    console.log(err)
  }
};

module.exports = postAndReplace;

// object looks like this = {movie : "", find : "", replace : ""}
