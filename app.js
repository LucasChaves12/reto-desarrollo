const Koa = require('koa');
const koaBody = require('koa-body');
const json = require('koa-json');
const mongoose = require('mongoose');
const router = require('./routes');

const app = new Koa();

const PORT = 3001

mongoose.connect('mongodb://localhost:27017/reto');

mongoose.connection.on('error', (err) => {
  console.log(err);
});


//Middleware to parse the body of the request
app.use(koaBody());

//Route middleware
app.use(router())


//Middleware to make the json prettier
app.use(json());

app.listen(PORT, () => {
  console.log(`Server is listening on port ${PORT}`);
});

